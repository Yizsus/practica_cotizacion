package com.example.cotizacion;

import java.io.Serializable;
import java.util.Random;

public class Cotizacion implements Serializable {
    private int folio;
    private String descripcion;
    private  float valorAuto;
    private float porEnganche;
    private  int plazos;

    //Constructores

    public Cotizacion(int folio, String descripcion, float valorAuto, float porEnganche, int plazos){
        this.folio = this.generarFolio();
        this.descripcion = descripcion;
        this.valorAuto = valorAuto;
        this.porEnganche = porEnganche;
        this.plazos = plazos;
    }
    public Cotizacion(){
        this.folio = this.generarFolio();
    }
    public int getFolio(){
        return folio;
    }
    public String getDescripcion(){
        return descripcion;
    }
    public  float getValorAuto(){
        return valorAuto;
    }
    public float getPorEnganche(){
        return porEnganche;
    }
    public int getPlazos(){
        return plazos;
    }
    public void setFolio(int folio){
        this.folio = folio;
    }
    public void setDescripcion(String descripcion){
        this.descripcion=descripcion;
    }
    public void setValorAuto(float valorAuto){
        this.valorAuto=valorAuto;
    }
    public void setPorEnganche(float porEnganche){
        this.porEnganche=porEnganche;
    }
    public void setPlazos(int plazos){
        this.plazos=plazos;
    }
    public int generarFolio(){
        return new Random().nextInt(500)%1000;
    }
    public float getcalcularPagoMensual(){return calcularPagoMensual();}
    public float getcalcularEnganche(){return calcularEnganche();}

    public float calcularEnganche(){
        return this.valorAuto*(this.porEnganche/100);
    }

    public float calcularPagoMensual(){
        float enganche = this.calcularEnganche();
        float totalFin = this.valorAuto-enganche;

        return totalFin/this.plazos;
    }
}
