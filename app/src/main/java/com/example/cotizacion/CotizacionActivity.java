package com.example.cotizacion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class CotizacionActivity extends AppCompatActivity {
    private Cotizacion cotizacion;
    private TextView lblFolio;
    private TextView lblNombre;
    private Button btnRegresar;
    private Button btnLimpiar;
    private Button btnCalcular;
    private EditText txtDescripcion;
    private EditText txtValorAuto;
    private EditText txtValorPago;
    private RadioButton rd12meses;
    private RadioButton rd18meses;
    private RadioButton rd24meses;
    private RadioButton rd36meses;
    private TextView lblValorPago;
    private TextView lblEnganche;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        lblFolio = (TextView) findViewById(R.id.lblFolio);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        txtDescripcion = (EditText) findViewById(R.id.txtDescripcion);
        txtValorAuto = (EditText) findViewById(R.id.txtValorAuto);
        txtValorPago = (EditText) findViewById(R.id.txtValorPago);
        rd12meses = (RadioButton) findViewById(R.id.rd12meses);
        rd18meses = (RadioButton) findViewById(R.id.rd18meses);
        rd24meses = (RadioButton) findViewById(R.id.rd24meses);
        rd36meses = (RadioButton) findViewById(R.id.rd36meses);
        lblValorPago = (TextView) findViewById(R.id.lblValorPago);
        lblEnganche = (TextView) findViewById(R.id.lblEnganche);
        Bundle datos = getIntent().getExtras();
        lblNombre.setText("Cliente: " + datos.getString("cliente"));

        cotizacion = (Cotizacion) datos.getSerializable("cotizacion");
        lblFolio.setText("Folio: " + String.valueOf(cotizacion.getFolio()));

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtDescripcion.setText("");
                txtValorAuto.setText("");
                txtValorPago.setText("");
                rd12meses.setChecked(true);
                rd18meses.setChecked(false);
                rd24meses.setChecked(false);
                rd36meses.setChecked(false);
                lblValorPago.setText("");
                lblEnganche.setText("");

            }
        });
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtValorAuto.getText().toString().isEmpty() || txtValorPago.getText().toString().isEmpty() || txtDescripcion.getText().toString().isEmpty()) {
                    Toast.makeText(CotizacionActivity.this, "Algunos campos vacíos", Toast.LENGTH_SHORT).show();
                } else {
                    cotizacion.setValorAuto(Float.parseFloat(txtValorAuto.getText().toString()));
                    cotizacion.setPorEnganche(Float.parseFloat(txtValorPago.getText().toString()));
                    if (rd12meses.isChecked()) {
                        cotizacion.setPlazos(12);
                    } else {
                        if (rd18meses.isChecked()) {
                            cotizacion.setPlazos(18);
                        } else {
                            if (rd24meses.isChecked()) {
                                cotizacion.setPlazos(24);
                            } else {
                                if (rd36meses.isChecked()) {
                                    cotizacion.setPlazos(36);
                                }
                            }
                        }
                    }

                    lblValorPago.setText("Pago mensual es: " + String.valueOf(cotizacion.getcalcularPagoMensual()));
                    lblEnganche.setText("Enganche es: " + String.valueOf(cotizacion.getcalcularEnganche()));
                }
            }
        });
    }

}
